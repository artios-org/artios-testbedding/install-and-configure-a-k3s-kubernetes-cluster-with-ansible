# Install and configure a k3s Kubernetes cluster with Ansible

This guide will configure Google Cloud's Compute Engine (GCE) instances (also called "virtual machines (VMs)") to install and configure a k3s Kubernetes cluster with Ansible.

## Relative work

- [Manage Google Cloud Compute Engine instances with Terraform](https://gitlab.com/artios-org/artios-testbedding/manage-google-cloud-compute-engine-instances-with-terraform)

## Prerequisites

- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/installation_distros.html) must be installed.

## Guide

### Create the directories structure

At the root level of your workspace, create two directories:

- [`ansible`](./ansible/): Store the Ansible configuration
- [`credentials`](./credentials/): Store the credentials information

### Generate the private/public SSH keys pair

Generate the private/public SSH keys pair that will be used to access the instances in the [`credentials`](./credentials/) directory.

```sh
# Create a new pair of SSH keys
ssh-keygen \
    -t ed25519 \
    -f ./credentials/gce-root-ssh-key \
    -q \
    -N "" \
    -C ""
```

Create the [`.gitignore`](./credentials/.gitignore) file to avoid credentials leaks.

### Create the Ansible configuration

Create the Ansible configuration files in the [`ansible`](./ansible/) directory.

Create the [`.gitignore`](./ansible/.gitignore) file.

Create the [`playbook.yml`](./ansible/playbook.yml) file.

Create a [`hosts`](./ansible/hosts) file with the following content (edit where needed):

```ini
[artio_nodes]
IP_FOR_NODE_1_CHANGE_ME
IP_FOR_NODE_2_CHANGE_ME
IP_FOR_NODE_3_CHANGE_ME
```

The Ansible inventory file contains all the hosts that are part of our infrastructure.

### Configure the infrastructure locally

_The following commands are run in the [`ansible`](./ansible/) directory._

Install and configure the instances.

```sh
# Run the Ansible Playbook
ansible-playbook --user "CHANGE_ME" --private-key "../credentials/gce-root-ssh-key" --inventory "hosts" playbook.yml
```

If no errors occur, you have successfully managed to configure the instances on Google Cloud using Ansible.

### Configure the infrastructure with GitLab CI/CD

Ansible has successfully been used locally to configure our infrastructure. We will now use GitLab CI/CD to configure our infrastructure with the rest of the team so anyone can configure  the infrastructure.

Create the [`.gitlab-ci.yml`](./.gitlab-ci.yml) file.

Transform the content of credentials files into base64 and save the output of the commands, it will be used later.

```sh
# Transform the private SSH key to base64
base64 ./credentials/gce-root-ssh-key
```

[Add the CI/CD environment variables to GitLab CI/CD](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) by going to the GitLab project. On the left sidebar, select **Settings > CI/CD**. Select **Variables** and add the following variables (mask and protect all variables for security reasons):

- `GCE_ROOT_SSH_PRIVATE_KEY`: The content in base64 of the `gce-root-ssh-key` file
- `GCE_ROOT_USERNAME`: The root username of the instance

Push the changes to GitLab. A [pipeline](https://gitlab.com/artios-org/artios-testbedding/install-and-configure-a-k3s-kubernetes-cluster-with-ansible/-/pipelines) should start with the following jobs:

- `ansible-playbook`: Configure the instances with Ansible. This job is manual.

If no errors occur, you have successfully managed to configure the instances on Google Cloud using Terraform and GitLab CI/CD.

## Additional resources

- [Ansible](https://www.ansible.com/) - Ansible is the simplest way to automate apps and IT infrastructure. Application Deployment + Configuration Management + Continuous Delivery.
- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) - GitLab CI/CD is a tool for software development using the continuous methodologies: Continuous Integration (CI), Continuous Delivery (CD) and Continuous Deployment (CD).

## License

This work is licensed under the [Non-Profit Open Software License version 3.0](https://opensource.org/licenses/NPOSL-3.0).
